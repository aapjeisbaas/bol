import json
import bol
from pprint import pprint
from decimal import Decimal


with open("config.json", "rb") as f:
    config = json.load(f)['shop_config']

BolShop = bol.Client(client_id=config['client_id'], client_secret=config['client_secret'])

open_orders = BolShop._orders()

pprint(open_orders)
print("----")
print("")

for order in open_orders:
    order_details = json.loads(json.dumps(BolShop._order(orderId=order['orderId'])), parse_float=Decimal)
    pprint(order_details)